﻿using EPiServer.DataAnnotations;
using Valtech.Templates.SalesLifeOnline;

namespace Valtech.Templates.SalesLifeOnline.Models
{
    /// <summary>
    /// Attribute used for site content types to set default attribute values
    /// </summary>
    public class SiteContentType : ContentTypeAttribute
    {
        public SiteContentType()
        {
            GroupName = Global.GroupNames.Default;
        }
    }
}