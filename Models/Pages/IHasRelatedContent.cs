using EPiServer.Core;

namespace Valtech.Templates.SalesLifeOnline.Models.Pages
{
    public interface IHasRelatedContent
    {
        ContentArea RelatedContentArea { get; }
    }
}