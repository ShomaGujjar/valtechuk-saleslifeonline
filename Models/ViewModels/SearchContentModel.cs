﻿using System.Collections.Generic;
using EPiServer.Find.UnifiedSearch;
using Valtech.Templates.SalesLifeOnline.Models.Pages;

namespace Valtech.Templates.SalesLifeOnline.Models.ViewModels
{
    public class SearchContentModel : PageViewModel<SearchPage>
    {
        public UnifiedSearchResults Results { get; set; }

        public string SearchQuery { get; set; }

        public SearchContentModel(SearchPage currentPage, string searchQuery) : base(currentPage)
        {
            SearchQuery = searchQuery;
        }

        //public bool SearchServiceDisabled { get; set; }
        
        //public int NumberOfHits { get; set; }
        //public IEnumerable<SearchHit> Hits { get; set; }  

        //public class SearchHit
        //{
        //    public string Title { get; set; }
        //    public string Url { get; set; }
        //    public string Excerpt { get; set; }
        //}
    }
}