﻿using System.Web.Mvc;
using EPiServer.Core;
using EPiServer.Web.Mvc;
using Valtech.Templates.SalesLifeOnline.Models.Pages;
using Valtech.Templates.SalesLifeOnline.Models.ViewModels;

namespace Valtech.Templates.SalesLifeOnline.Controllers
{
    public class StartPageController : PageControllerBase<StartPage>
    {
        //public string Index(StartPage currentPage)
        //{
        //    //var model = PageViewModel.Create(currentPage);

        //    //if (ContentReference.StartPage.CompareToIgnoreWorkID(currentPage.ContentLink)) // Check if it is the StartPage or just a page of the StartPage type.
        //    //{
        //    //    //Connect the view models logotype property to the start page's to make it editable
        //    //    var editHints = ViewData.GetEditHints<PageViewModel<StartPage>, StartPage>();
        //    //    editHints.AddConnection(m => m.Layout.Logotype, p => p.SiteLogotype);
        //    //    editHints.AddConnection(m => m.Layout.ProductPages, p => p.ProductPageLinks);
        //    //    editHints.AddConnection(m => m.Layout.CompanyInformationPages, p => p.CompanyInformationPageLinks);
        //    //    editHints.AddConnection(m => m.Layout.NewsPages, p => p.NewsPageLinks);
        //    //    editHints.AddConnection(m => m.Layout.CustomerZonePages, p => p.CustomerZonePageLinks);
        //    //}

        //    //return View(model);
        //    return "Hello Sales Life";
        //}
        public ViewResult Index()
        {
            return View("Index");
        }

    }
}
