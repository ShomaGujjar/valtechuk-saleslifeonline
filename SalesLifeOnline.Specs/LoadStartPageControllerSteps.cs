﻿using System;
using System.Web.Mvc;
using NUnit.Core;
using NUnit.Framework;
using SpecBind.ActionPipeline;
using TechTalk.SpecFlow;
using Valtech.Templates.SalesLifeOnline.Controllers;

namespace SalesLifeOnline.Specs
{
    [Binding]
    public class LoadStartPageControllerSteps
    {
        ViewResult _result;
        StartPageController _controller;

        [When(@"the user goes to the home page")]
        public void WhenTheUserGoesToTheHomePage()
        {
            _controller=new StartPageController();
            _result = _controller.Index();
        }
        
        [Then(@"the user view Hello Sales Life Online should be displayed")]
        public void ThenTheUserViewHelloSalesLifeOnlineShouldBeDisplayed()
        {
            Assert.IsInstanceOf<ViewResult>(_result);
            Assert.AreEqual(((ViewResult)_result).ViewName,"Index");            
        }
    }
}
